require 'editor_configuration'    -- Configuration like encoding, display number lines ...
require 'packer_plugins_tracking' -- Plugins manage by packer
require 'plugins_configs'         -- Configuration of plugins intalled
require 'shortcut'                -- Nvim or plugin's shortcut 
