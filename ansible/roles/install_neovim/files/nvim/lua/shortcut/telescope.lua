-- Key for telescope and telescope-file-browser
local builtin = require('telescope.builtin')
local browser = require('telescope').extensions.file_browser.file_browser

vim.keymap.set('n', '<space>ff', builtin.find_files, {})
vim.keymap.set('n', '<space>fg', builtin.live_grep, {})
vim.keymap.set('n', '<space>fb', builtin.buffers, {})
vim.keymap.set('n', '<space>fh', builtin.help_tags, {})
vim.keymap.set('n', '<space>fe', browser, {})